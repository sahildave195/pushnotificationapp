[Test Link](https://bitbucket.org/sahildave195/codelearntwittertest)

I have built up the project from the challenge's last level. Parse.com was not really working well with Robolectric. I tried to use GCM too but I had this intuition from the beginning that we'd finally need Twitter4j to check for the "mentions". 

So I created the Twitter4j Stream service which runs in the background all the time. It gets the statuses from the user's feed and reads them to see if there are ANY mentions in them. If yes, it scans that status itself to find if the logged-in user's screen name is also mentioned or not. If yes, a notification is displayed on the phone. The notification currently contains:

* Icon as the app icon
* Title as "<someone> mentioned you"
* Notification Text shows the tweet.

On clicking the notification, the app goes directly to the "TweetDetailActivity" showing the tweet.

We can also add three buttons below the notification like the official twitter app, image below:

![](http://i.imgur.com/RlvExPx.png) 

The first notification is from the official Twitter app, and second is from Codelearn's. We can change the title to user's name too but as far as I know, the handle is more preferred in Twitter.

### About App

I have added two classes in `org.codelearn.twitter.util;` and also moved the `TwitterConstants` class to this package for consistency.

The stream service is called when the user gets his authentication done. (Line 148 in MainActivity). The `StreamReaderService` class creates a `TwitterStream` with the help of `TwitterStreamBuilderUtil` class. Listener is attached on the stream and it starts scanning the user stream by `stream.user();` command at Line 118.


### About Test

Testing on notifications is limited. [Official Example](https://github.com/robolectric/robolectric/blob/master/src/test/java/org/robolectric/shadows/NotificationManagerTest.java). Building up the notification in test is similar to building it in the actual app. Testing is done to check the *nullness* of the notification manager and the size of notification to be 1. Cancelling the notificaion is also checked.

`performClick` or `Robolectric.clickOn` is not valid for notifications so we need to find another way to check the intent. Due to the lack on `performClick` I couldnt do tests on the next activity i.e. the TweetDetailActivity. 

I am still trying different ways to tackle this problem but I think we'd need something else than Robolectric to do this.

### Setup.

In the app, apart from the two new classes, only `twitter4j-stream-4.0.1.jar` library is new. 

For the test, I had used [this guide](http://www.peterfriese.de/unit-testing-android-apps-with-robolectric-and-eclipse/) if that makes a difference. Otherwise there is nothing peculiar about this one class project.

[Test Link](https://bitbucket.org/sahildave195/codelearntwittertest)
package org.codelearn.twitter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.codelearn.twitter.util.StreamReaderService;
import org.codelearn.twitter.util.TwitterConstants;
import org.json.JSONException;
import org.json.JSONObject;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

public class MainActivity extends Activity {
	public static final String TAG = "Codelearn";
	private Button _loginBtn;
	private RelativeLayout loginProgress;
	private String urlString = "http://app-dev-challenge-endpoint.herokuapp.com/login";

	private String usernameString;
	private String passwordString;

	private SharedPreferences tweetPrefs;
	private Editor prefEdit;
	private Twitter twitter;
	private static RequestToken requestToken;
	private String oaVerifier;
	private boolean loggedIn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		_loginBtn = (Button) findViewById(R.id.btn_login);
		loginProgress = (RelativeLayout) findViewById(R.id.loginProgressBarLayout);

		tweetPrefs = getSharedPreferences(TwitterConstants.SHARED_PREFERENCE,
				MODE_PRIVATE);
		prefEdit = tweetPrefs.edit();

		loggedIn = tweetPrefs.getBoolean(
				TwitterConstants.PREF_KEY_TWITTER_LOGIN, false);

		ConfigurationBuilder builder = new ConfigurationBuilder();
		builder.setOAuthConsumerKey(TwitterConstants.CONSUMER_KEY);
		builder.setOAuthConsumerSecret(TwitterConstants.CONSUMER_SECRET);
		Configuration configuration = builder.build();

		TwitterFactory factory = new TwitterFactory(configuration);
		twitter = factory.getInstance();

		Log.d(TAG, "onCreate loggedIn ? " + loggedIn);

		String s = tweetPrefs.getString(TwitterConstants.PREF_KEY_SECRET, null);
		String s1 = tweetPrefs.getString(TwitterConstants.PREF_KEY_TOKEN, null);
		if (s != null && s1 != (null)) {
			Intent intent = new Intent(MainActivity.this,
					TweetListActivity.class);
			startActivity(intent);
			finish();
		} else {
			Uri twitURI = getIntent().getData();
			// make sure the url is correct
			Log.d(TAG, "not logged");

			if (twitURI != null
					&& twitURI.toString().startsWith(
							TwitterConstants.CALLBACK_URL)) {

				oaVerifier = twitURI.getQueryParameter("oauth_verifier");
				new GetAuthentication().execute(oaVerifier);
			}
		}

		_loginBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Log.d(TAG, "onclick");

				new login().execute();
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	private class GetAuthentication extends AsyncTask<String, Void, Void> {

		@Override
		protected Void doInBackground(String... params) {

			try {

				AccessToken accessToken = twitter.getOAuthAccessToken(
						requestToken, oaVerifier);

				// User in now verified, save info
				prefEdit.putString(TwitterConstants.PREF_KEY_TOKEN,
						accessToken.getToken());
				prefEdit.putString(TwitterConstants.PREF_KEY_SECRET,
						accessToken.getTokenSecret());

				// login info
				prefEdit.putBoolean(TwitterConstants.PREF_KEY_TWITTER_LOGIN,
						true);
				prefEdit.commit();

				long id = twitter.getId();
				User user = twitter.showUser(id);
				Log.d("Mentioned", user.getScreenName().toString());

				
				//Calling the StreamService
				StreamReaderService readerService = new StreamReaderService(
						MainActivity.this, user.getScreenName().toString());

				Intent intent = new Intent(MainActivity.this,
						TweetListActivity.class);
				startActivity(intent);
				finish();

			} catch (TwitterException te) {
				Log.e(TAG, "Failed to get access token: " + te.getMessage());
			}
			return null;
		}

	}

	private class login extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			loginProgress.setVisibility(View.VISIBLE);

		}

		@Override
		protected Void doInBackground(Void... params) {
			if (!loggedIn) {

				try {
					requestToken = twitter
							.getOAuthRequestToken(TwitterConstants.CALLBACK_URL);
					startActivity(new Intent(Intent.ACTION_VIEW,
							Uri.parse(requestToken.getAuthenticationURL())));
				} catch (TwitterException e) {
					e.printStackTrace();
				}
			} else {
				// user already logged into twitter
				Log.d(TAG, "Already Logged in");
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);

			loginProgress.setVisibility(View.GONE);
		}

	}

	private class GetTokenTask extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			loginProgress.setVisibility(View.VISIBLE);
		}

		@Override
		protected Boolean doInBackground(Void... params) {
			Log.d(TAG, "in doInBackground");
			// URI url;

			try {
				Log.d(TAG, "gettingResponse");
				// url = new URL(urlString);

				HttpClient client = new DefaultHttpClient();
				HttpPost post = new HttpPost(urlString);

				// HttpURLConnection con = (HttpURLConnection) url
				// .openConnection();
				// con.setDoOutput(true);
				// con.setDoInput(true);

				// con.setRequestProperty("Content-Type", "application/json");
				// con.setRequestProperty("Accept", "application/json");
				// con.setRequestMethod("POST");

				post.setHeader("Content-type", "application/json");
				post.setHeader("Accept", "application/json");

				// OutputStreamWriter wr = new OutputStreamWriter(
				// con.getOutputStream());

				JSONObject cred = new JSONObject();
				cred.put("username", usernameString);
				cred.put("password", passwordString);

				StringEntity se = new StringEntity(cred.toString());
				se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
						"application/json"));
				post.setEntity(se);

				// wr.write(cred.toString());
				// wr.flush();

				HttpResponse response = client.execute(post);

				// GET
				StringBuilder sb = new StringBuilder();
				// int HttpResult = con.getResponseCode();
				BufferedReader br = new BufferedReader(new InputStreamReader(
						response.getEntity().getContent()));

				String line = null;

				while ((line = br.readLine()) != null) {
					sb.append(line + "\n");
				}

				br.close();

				Log.d(TAG, "" + sb.toString());

				return true;

			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			}
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			Log.d(TAG, "onPostExecute");

			if (result) {
				Log.d(TAG, "gotToken");
				SharedPreferences prefs = getSharedPreferences(
						"codelearn_twitter", MODE_PRIVATE);
				Editor edit = prefs.edit();
				edit.putString("username", usernameString);
				edit.commit();
				edit.putString("password", passwordString);
				edit.commit();

				Intent intent = new Intent(MainActivity.this,
						TweetListActivity.class);
				startActivity(intent);
				loginProgress.setVisibility(View.GONE);
				finish();
			} else {
				Log.d(TAG, "unable to get token");

			}
		}

	}

}

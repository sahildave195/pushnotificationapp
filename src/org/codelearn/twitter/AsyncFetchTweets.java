package org.codelearn.twitter;

import java.util.ArrayList;
import java.util.List;

import org.codelearn.twitter.models.Tweet;
import org.codelearn.twitter.util.TwitterConstants;

import twitter4j.Paging;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.conf.ConfigurationBuilder;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

public class AsyncFetchTweets extends AsyncTask<Void, Void, List<Tweet>> {
	public static final String TAG = "Codelearn";
	private List<Tweet> tweets = new ArrayList<Tweet>();
	private TweetListActivity listActivity = null;
	private SharedPreferences tweetPrefs;
	private List<twitter4j.Status> statuses = new ArrayList<twitter4j.Status>();

	public AsyncFetchTweets(TweetListActivity act) {
		listActivity = act;
		tweetPrefs = listActivity.getSharedPreferences(
				TwitterConstants.SHARED_PREFERENCE, 0);
	}

	@Override
	protected List<Tweet> doInBackground(Void... params) {

		try {
			Log.d(TAG, "in do in Background");

			ConfigurationBuilder builder = new ConfigurationBuilder();
			long sinceIdDefValue = 1L;

			String access_token = tweetPrefs.getString(
					TwitterConstants.PREF_KEY_TOKEN, "");
			String access_token_secret = tweetPrefs.getString(
					TwitterConstants.PREF_KEY_SECRET, "");
			Long oldLastSinceId = tweetPrefs.getLong(
					TwitterConstants.TWITTER_SINCEID, sinceIdDefValue);

			// DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			//
			// Calendar calendar = Calendar.getInstance();
			// calendar.setTimeInMillis(oldLastSinceId);
			// String oldDateString = formatter.format(calendar.getTime());
			// String queryString = "count:25 since:" + oldDateString;

			Log.d(TAG, access_token);
			Log.d(TAG, access_token_secret);
			Log.d(TAG, oldLastSinceId + "");

			builder.setOAuthConsumerKey(TwitterConstants.CONSUMER_KEY);
			builder.setOAuthConsumerSecret(TwitterConstants.CONSUMER_SECRET);
			builder.setOAuthAccessToken(access_token);
			builder.setOAuthAccessTokenSecret(access_token_secret);

			AccessToken accessToken = new AccessToken(access_token,
					access_token_secret);
			Twitter twitter = new TwitterFactory(builder.build())
					.getInstance(accessToken);

			// Query query = new Query().since(queryString);
			// Query query = new Query();
			// QueryResult result = twitter.search(query);
			//
			// statuses = result.getTweets();
			// Log.d(TAG, "size - " + statuses.size());
			// Log.d(TAG, "maxId - " + result.getMaxId());

			Paging paging = new Paging(oldLastSinceId);

			statuses = twitter.getHomeTimeline(paging);

			for (twitter4j.Status status : statuses) {
				Log.d(TAG, status.getUser().getName() + ":" + status.getText());

				Tweet tweet = new Tweet();
				tweet.setTitle(status.getUser().getScreenName());
				tweet.setBody(status.getText());
				tweets.add(tweet);

			}

			// Date newlastUpdated = new Date();
			// Long dateInLong = newlastUpdated.getTime();

			if (statuses.size() > 0) {

				long newSinceId = statuses.get(0).getId();

				tweetPrefs.edit()
						.putLong(TwitterConstants.TWITTER_SINCEID, newSinceId)
						.commit();
				Log.d(TAG, "saving - " + newSinceId);

			}

		} catch (TwitterException e) {
			// Error in updating status
			Log.d(TAG, e.getMessage());
		}
		return tweets;
	}

	protected void onPostExecute(List<Tweet> TweetRead) {
		Log.d(TAG, "onPostExecute " + TweetRead.size());
		listActivity.renderTweets(tweets);

	}
}

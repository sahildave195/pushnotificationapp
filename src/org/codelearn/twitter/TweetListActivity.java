package org.codelearn.twitter;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import org.codelearn.twitter.models.Tweet;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class TweetListActivity extends ListActivity {
	public static final String TAG = "Codelearn";
	private TweetAdapter tweetItemArrayAdapter;
	private List<Tweet> tweets = new ArrayList<Tweet>();
	private List<Tweet> tweetsRead = new ArrayList<Tweet>();

	private static final String TWEETS_CACHE_FILE = "tweet_cache.ser";
	private static final long serialVersionUID = 1L;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tweet_list);

		Log.d(TAG, "TweetListAct");

		// try {
		// FileInputStream fis = openFileInput(TWEETS_CACHE_FILE);
		// ObjectInputStream ois = new ObjectInputStream(fis);
		// tweets = (List<Tweet>) ois.readObject();
		// Log.d(TAG, "tweets - " + tweets.size());
		//
		// } catch (Exception e) {
		// Log.d(TAG, "No cache - " + e.getMessage().toString());
		// }

		SharedPreferences sharedPref = getSharedPreferences("tweetList",
				Context.MODE_PRIVATE);
		String defValueTweetList = "0";

		String str = sharedPref.getString("tweetsOld", defValueTweetList);
		Log.d(TAG, "got list as - " + str);
		if (!str.equals("0")) {

			Gson gson = new Gson();
			Type type = new TypeToken<List<Tweet>>() {
			}.getType();

			tweets = gson.fromJson(str, type);
		}
		tweetItemArrayAdapter = new TweetAdapter(this, tweets);
		setListAdapter(tweetItemArrayAdapter);
		refreshlist();

	}

	public void refreshlist() {
		AsyncFetchTweets asnyc = new AsyncFetchTweets(this);
		asnyc.execute();
	}

	public void renderTweets(List<Tweet> tweetsInput) {
		Log.d(TAG, "rendering");

		for (int i = tweetsInput.size() - 1; i >= 0; i--) {
			tweets.add(0, tweetsInput.get(i));
		}

		AsyncWriteTweets test1 = new AsyncWriteTweets(this);
		test1.execute(tweets);

		// tweets.addAll(0, tweetsInput);
		tweetItemArrayAdapter.notifyDataSetChanged();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.tweet_list, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_refresh) {
			// sign in button pressed
			Log.d(TAG, "refreshing");
			refreshlist();
			return true;
		}

		if (id == R.id.action_compose) {
			Intent intent = new Intent(TweetListActivity.this,
					ComposeTweetActivity.class);
			startActivity(intent);

			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		Intent intent = new Intent(this, TweetDetailActivity.class);

		intent.putExtra("MyClass", (Tweet) getListAdapter().getItem(position));
		startActivity(intent);

	}
}

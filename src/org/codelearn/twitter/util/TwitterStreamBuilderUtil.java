package org.codelearn.twitter.util;

import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.conf.ConfigurationBuilder;
import android.content.Context;
import android.content.SharedPreferences;

public class TwitterStreamBuilderUtil {

	Context mContext;

	public TwitterStreamBuilderUtil(Context mContext) {
		System.out.println("TwitterStreamBuilderUtil");
		this.mContext = mContext;

	}

	public TwitterStream getStream() {
		System.out.println("getStream");
		ConfigurationBuilder builder = new ConfigurationBuilder();

		SharedPreferences tweetPrefs = mContext.getSharedPreferences(
				TwitterConstants.SHARED_PREFERENCE, 0);

		String access_token = tweetPrefs.getString(
				TwitterConstants.PREF_KEY_TOKEN, "");
		String access_token_secret = tweetPrefs.getString(
				TwitterConstants.PREF_KEY_SECRET, "");

		builder.setOAuthConsumerKey(TwitterConstants.CONSUMER_KEY);
		builder.setOAuthConsumerSecret(TwitterConstants.CONSUMER_SECRET);
		builder.setOAuthAccessToken(access_token);
		builder.setOAuthAccessTokenSecret(access_token_secret);

		return new TwitterStreamFactory(builder.build()).getInstance();

	}

}

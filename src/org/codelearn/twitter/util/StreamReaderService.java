package org.codelearn.twitter.util;

import org.codelearn.twitter.R;
import org.codelearn.twitter.TweetDetailActivity;
import org.codelearn.twitter.models.Tweet;

import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import twitter4j.TwitterStream;
import twitter4j.UserMentionEntity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

public class StreamReaderService {

	Context mContext;
	String loggedUser;

	public StreamReaderService(Context context, String user) {
		System.out.println("StreamReaderService");
		this.mContext = context;
		this.loggedUser = user;
		readerService();
	}

	public void readerService() {

		System.out.println("readerService");

		TwitterStreamBuilderUtil streamBuilderUtil = new TwitterStreamBuilderUtil(
				mContext);
		TwitterStream stream = streamBuilderUtil.getStream();

		StatusListener listener = new StatusListener() {

			@Override
			public void onStatus(Status status) {

				//If there are mentions in one status, loop on that status to check all the 
				// if the mention is about "loggedUser"
				for (UserMentionEntity mention : status
						.getUserMentionEntities()) {
					if (mention.getScreenName().equals(loggedUser)) {

						Log.d("Mentioned", "GOT MENTION");
						Log.d("Mentioned", status.getUser().getScreenName()
								+ " - " + status.getText());

						String notiTitle = status.getUser().getScreenName()
								+ " mentioned you";
						String notiText = status.getText().trim();

						Tweet notiTweet = new Tweet();
						notiTweet.setTitle(notiTitle);
						notiTweet.setBody(notiText);

						Intent intent = new Intent(mContext,
								TweetDetailActivity.class);
						intent.putExtra("MyClass", notiTweet);
						int requestID = (int) System.currentTimeMillis();
						int flags = PendingIntent.FLAG_CANCEL_CURRENT;
						PendingIntent pIntent = PendingIntent.getActivity(
								mContext, requestID, intent, flags);

						NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
								mContext).setSmallIcon(R.drawable.ic_launcher)
								.setContentTitle(notiTitle)
								.setContentIntent(pIntent).setAutoCancel(true)
								.setContentText(notiText);

						NotificationManager mNotificationManager = (NotificationManager) mContext
								.getSystemService(Context.NOTIFICATION_SERVICE);
						mNotificationManager.notify(
								TwitterConstants.NOTIFY_MENTION,
								mBuilder.build());

					}
				}
			}

			@Override
			public void onDeletionNotice(
					StatusDeletionNotice statusDeletionNotice) {
			}

			@Override
			public void onTrackLimitationNotice(int numberOfLimitedStatuses) {
				System.out.println("Got track limitation notice:"
						+ numberOfLimitedStatuses);
			}

			@Override
			public void onScrubGeo(long userId, long upToStatusId) {
				System.out.println("Got scrub_geo event userId:" + userId
						+ " upToStatusId:" + upToStatusId);
			}

			@Override
			public void onStallWarning(StallWarning warning) {
				System.out.println("Got stall warning:" + warning);
			}

			@Override
			public void onException(Exception ex) {
				ex.printStackTrace();
			}
		};

		// Add the above listener to the stream received from
		// TwitterStreamBuilderUtil
		stream.addListener(listener);
		stream.user();

	}

}

package org.codelearn.twitter;

import org.codelearn.twitter.util.TwitterConstants;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.conf.ConfigurationBuilder;
import android.app.Activity;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ComposeTweetActivity extends Activity {

	private EditText compose;
	private Button btn_submit;
	private SharedPreferences tweetPrefs;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_compose_tweet);

		tweetPrefs = getSharedPreferences(TwitterConstants.SHARED_PREFERENCE,
				MODE_PRIVATE);

		compose = (EditText) findViewById(R.id.fld_compose);
		btn_submit = (Button) findViewById(R.id.btn_submit);

		btn_submit.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				String status = compose.getText().toString();

				if (status.trim().length() > 0) {
					new updateTwitterStatus().execute(status);
				} else {
					Toast.makeText(getApplicationContext(),
							"Please write something!", Toast.LENGTH_SHORT)
							.show();
				}
			}
		});

	}

	class updateTwitterStatus extends AsyncTask<String, String, String> {

		protected String doInBackground(String... args) {

			String status = args[0];
			try {
				ConfigurationBuilder builder = new ConfigurationBuilder();

				// Access Token
				String access_token = tweetPrefs.getString(
						TwitterConstants.PREF_KEY_TOKEN, "");
				String access_token_secret = tweetPrefs.getString(
						TwitterConstants.PREF_KEY_SECRET, "");

				builder.setOAuthConsumerKey(TwitterConstants.CONSUMER_KEY);
				builder.setOAuthConsumerSecret(TwitterConstants.CONSUMER_SECRET);
				builder.setOAuthAccessToken(access_token);
				builder.setOAuthAccessTokenSecret(access_token_secret);

				AccessToken accessToken = new AccessToken(access_token,
						access_token_secret);
				Twitter twitter = new TwitterFactory(builder.build())
						.getInstance(accessToken);

				// Update status
				twitter.updateStatus(status);
				

			} catch (TwitterException e) {
				// Error in updating status
				Log.d(MainActivity.TAG, e.getMessage());
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			finish();

		}

	}
}

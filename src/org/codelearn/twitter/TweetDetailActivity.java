package org.codelearn.twitter;

import org.codelearn.twitter.models.Tweet;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.widget.TextView;

public class TweetDetailActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tweet_detail);

		TextView u1 = (TextView) findViewById(R.id.tweetTitle);
		TextView u2 = (TextView) findViewById(R.id.tweetBody);

		Tweet value = (Tweet) getIntent().getSerializableExtra("MyClass");
		System.out.println(value.toString());
		u1.setText(value.getTitle().toString());
		u2.setText(value.getBody().toString());

		// NotificationManager mNotificationManager = (NotificationManager)
		// getSystemService(Context.NOTIFICATION_SERVICE);
		// mNotificationManager.cancel(TwitterConstants.NOTIFY_MENTION);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.tweet_detail, menu);
		return true;
	}

}

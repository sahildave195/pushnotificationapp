package org.codelearn.twitter;

import java.util.ArrayList;
import java.util.List;

import org.codelearn.twitter.models.Tweet;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;

public class AsyncWriteTweets extends AsyncTask<List<Tweet>, Void, Void> {
	private TweetListActivity listaActivity;
	public List<Tweet> tweetsRead = new ArrayList<Tweet>();

	public AsyncWriteTweets(TweetListActivity act) {
		this.listaActivity = act;
	}

	@Override
	protected Void doInBackground(List<Tweet>... tweets) {

		Log.d(MainActivity.TAG, " write Called()");

		Gson gson = new Gson();
		String tweetsOld = gson.toJson(tweets[0]);

		Log.d(MainActivity.TAG, "saving list as - " + tweetsOld);

		SharedPreferences tweetList = listaActivity.getSharedPreferences(
				"tweetList", Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = tweetList.edit();
		editor.putString("tweetsOld", tweetsOld);
		editor.commit();

		// try {
		//
		// Thread.sleep(5000);
		// Log.d(MainActivity.TAG, " write Called()");
		// test.getFileStreamPath(TWEETS_CACHE_FILE).delete();
		// FileOutputStream fis = test.openFileOutput(TWEETS_CACHE_FILE,
		// Context.MODE_PRIVATE);
		// ObjectOutputStream oos = new ObjectOutputStream(fis);
		// oos.writeObject(tweets);
		// oos.reset();
		// oos.close();
		// Log.d(MainActivity.TAG, test.getFileStreamPath(TWEETS_CACHE_FILE)
		// .getAbsolutePath().toString());
		// } catch (Exception e) {
		// Log.d(MainActivity.TAG, "" + e.toString());
		// }
		return null;
	}

}
